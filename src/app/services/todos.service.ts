import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Todo} from '../models/todo';

const httpOptions = {
  headers: new HttpHeaders().set('Content-Type', 'application/json')
};

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  constructor(private http: HttpClient) { }

  getTodos(): Promise<Todo[]> {
    return this.http.get(`${environment.apiUrl}todos`, httpOptions).toPromise() as Promise<Todo[]>;
  }
}
