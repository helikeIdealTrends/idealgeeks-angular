import { Component, OnInit } from '@angular/core';
import {TodosService} from '../../services/todos.service';
import {Todo} from '../../models/todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  public todos: Todo[];

  constructor(
    public todosService: TodosService
  ) { }

  async ngOnInit() {
    this.todos = await this.todosService.getTodos();
  }

}
